#include <stdio.h>
#include <stdlib.h>

typedef enum {
	false,true
} bool;

void swap(int *a, int *b)
{
	int temp;
	temp = *b;
	*b = *a;
	*a = temp;
	return;
}

int main(void)
{
	int n;
	printf("The number of elements in array (must be > 2): ");
	scanf("%i", &n);
	
	while(n<2)
	{
		printf("n must be larger than 2\nPlease retype n!\nn=");
		scanf("%i", &n);
	}
	
	int a[n];
	printf("Now you can create an array of integer with %i elements\n", n);
	printf("Then the software will help you sort the elements in order from small to large\n");
	
	for (int i=0; i<n; i++)
	{
		printf("a[%i] = ",i);
		scanf("%i", &a[i]);
	}
	
	printf("the array you create is: \n");
	
	for (int i=0; i<n; i++)
	{
		printf("%i ",a[i]);
	}
	
	bool check = true;
	
	while(check == true)
	{
		int count=0;
		for(int i=0; i<(n-1); i++)
		{
			if(a[i] > a[i+1])
			{
				count++;
				swap(&a[i], &a[i+1]);
			}
		}
		if(count==0) check = false;
	}
	
	printf("\n the array after sorted is: \n");
	
	for (int i=0; i<n; i++)
	{
		printf("%i ", a[i]);
	}
	
	
	return 0;
}